# Generated by Django 3.2 on 2022-09-20 01:28

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Mentorship',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('qna', models.JSONField()),
                ('mentor_id', models.CharField(max_length=100)),
                ('mentee_id', models.CharField(max_length=100)),
                ('status', models.CharField(choices=[('PENDING', 'PENDING'), ('ACCEPTED', 'ACCEPTED'), ('REJECTED', 'REJECTED')], default='PENDING', max_length=10)),
                ('available', models.DateTimeField()),
                ('available_hour', models.IntegerField()),
            ],
        ),
    ]
